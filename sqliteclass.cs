﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Login_Sql
{
    public class sqliteclass
    {
        //private String _dbFile = Application.StartupPath + Properties.Settings.Default.logindb;
        string _dbFile = Properties.Settings.Default.localdb; // local database
        //string _dbFile = Properties.Settings.Default.wifidb; // wireless database
       
        string connString;
        private SQLiteConnection SqldbConn;

        public sqliteclass()
        {
            connString = string.Format(@"Data Source={0}; Pooling=false; FailIfMissing=false;", _dbFile);
            SqldbConn = new System.Data.SQLite.SQLiteConnection(connString);
        }

        public String dbFile
        {
            get { return this._dbFile; }
            set { this._dbFile = value; }
        }

        public Boolean openConnection()
        {
            try
            {
                SqldbConn.Open();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public Boolean closeConnection()
        {
            try
            {
                SqldbConn.Close();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public int getUser(User myuser, String usernameText, String passwordText)
        {
            using (System.Data.SQLite.SQLiteCommand cmd = SqldbConn.CreateCommand())
            {
                cmd.CommandText = String.Format("SELECT id, username, active FROM userLogin WHERE username='{0}' AND password='{1}' LIMIT 1 ", usernameText, passwordText);
                openConnection();
                SQLiteDataReader sqReader = cmd.ExecuteReader();
                try
                {
                    while (sqReader.Read())
                    {
                        myuser.userID = sqReader.GetInt32(0);
                        myuser.uname = sqReader.GetString(1);                        
                        myuser.isactive = Convert.ToBoolean(sqReader.GetInt32(2));
                        // myuser.lastlogin = GetDateTime(sqReader.GetString(4));
                    }
                }
                finally
                {
                    sqReader.Close();
                    closeConnection();
                }
                return 1;
            }
        }

        public Boolean updateUserLastlogin(User myuser)
        {
            Boolean returnval = false;
            using (System.Data.SQLite.SQLiteCommand cmd = SqldbConn.CreateCommand())
            {
                try
                {
                    cmd.CommandText = "UPDATE userLogin SET lastlogin = @lastlogin WHERE id = @id";
                    cmd.Parameters.Add("@lastlogin", System.Data.DbType.String).Value = myuser.lastlogin.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
                    cmd.Parameters.Add("@id", System.Data.DbType.String).Value = myuser.userID;
                    openConnection();
                    cmd.ExecuteNonQuery();
                    returnval = true;
                }
                catch
                {
                    returnval = false;
                }
                finally
                {
                    closeConnection();
                }
            }
            return returnval;
        }

        public Boolean insertUserDatelogin(User myuser)
        {
            Boolean returnval = false;
            using (System.Data.SQLite.SQLiteCommand cmd = SqldbConn.CreateCommand())
            {
                try
                {
                    cmd.CommandText = "INSERT INTO userlog (username,datelogin,logmessage) VALUES (@username,@datelogin,@logmessage)";
                    cmd.Parameters.Add("@username", System.Data.DbType.String).Value = myuser.uname;                  
                    cmd.Parameters.Add("@datelogin", System.Data.DbType.String).Value = myuser.lastlogin.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
                    cmd.Parameters.Add("@logmessage", System.Data.DbType.String).Value = myuser.logmessage;
                    openConnection();
                   
                    SQLiteTransaction transaction = null;
                    transaction = SqldbConn.BeginTransaction();
                    cmd.ExecuteNonQuery();

                    long rowID = SqldbConn.LastInsertRowId;

                    transaction.Commit();
                    myuser.userID = (Int32)rowID;

                    returnval = true;
                }
                catch
                {
                    returnval = false;
                }
                finally
                {
                    closeConnection();
                }
            }
            return returnval;
        }

    }
}
