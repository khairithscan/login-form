﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Login_Sql
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            GlobalVar.thesqlconn = new sqliteclass();
        }

        private void buttonLogin_Click(object sender, EventArgs e)
        {
            String uname = textBoxUserName.Text;
            String upassword = textBoxPassword.Text;
            GlobalVar.curruser = new User();

            GlobalVar.thesqlconn.getUser(GlobalVar.curruser, uname, upassword);
           
            if (GlobalVar.curruser.isactive)
            {               
                labelStatus.Visible = true;
                labelStatus.ForeColor = Color.Green;
                labelStatus.Text = "Successful";

                GlobalVar.curruser.lastlogin = DateTime.Now;                                
                GlobalVar.curruser.logmessage = labelStatus.Text;
                GlobalVar.thesqlconn.updateUserLastlogin(GlobalVar.curruser);
                GlobalVar.thesqlconn.insertUserDatelogin(GlobalVar.curruser);
                this.DialogResult = DialogResult.OK;
                               
            }
            else
            {                             
                labelStatus.Visible = true;
                labelStatus.ForeColor = Color.Red;
                labelStatus.Text = "Unsuccessful";

                GlobalVar.curruser.uname = uname;
                GlobalVar.curruser.lastlogin = DateTime.Now;
                GlobalVar.curruser.logmessage = labelStatus.Text;
                GlobalVar.thesqlconn.insertUserDatelogin(GlobalVar.curruser);
            }
        }       
    }
}
