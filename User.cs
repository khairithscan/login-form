﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Login_Sql
{
    public class User
    {
        Int32 _userID;
        String _uname;
        String _upassword;
        Boolean _isactive;
        DateTime _lastlogin;
        DateTime _timelogin;
        String _logmessage;

        public User()
        {
            this._isactive = false;
        }

        public Int32 userID
        {
            get { return _userID; }
            set { _userID = value; }
        }
        public String uname
        {
            get { return _uname; }
            set { _uname = value; }
        }
        public String upassword
        {
            get { return _upassword; }
            set { _upassword = value; }
        }

        public DateTime lastlogin
        {
            get { return _lastlogin; }
            set { _lastlogin = value; }
        }

        public DateTime timelogin
        {
            get { return _timelogin; }
            set { _timelogin = value; }
        }

        public String logmessage
        {
            get { return _logmessage; }
            set { _logmessage = value; }
        }

        public Boolean isactive
        {
            get { return _isactive; }
            set { _isactive = value; }
        }
    }
}
